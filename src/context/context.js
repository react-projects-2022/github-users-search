import React, { useState, useEffect, createContext, useContext } from "react";
import mockUser from "./mockData.js/mockUser";
import mockRepos from "./mockData.js/mockRepos";
import mockFollowers from "./mockData.js/mockFollowers";
import axios from "axios";

const rootUrl = "https://api.github.com";

const GithubContext = createContext();

const GithubProvider = ({ children }) => {
  const [githubUser, setGithubUser] = useState(mockUser);
  const [repos, setRepos] = useState(mockRepos);
  const [followers, setFollowers] = useState(mockFollowers);
  // request
  const [request, setRequest] = useState(0);
  //loading
  // const [loading, setLoading] = useState(false);
  //error
  const [error, setError] = useState({ show: false, msg: "" });
  //search user

  const searchUser = async (user) => {
    //toggle error
    //loading
    const response = await axios(`${rootUrl}/users/${user}`).catch((error) =>
      console.log(error)
    );
    if (response) {
      setGithubUser(response.data);
      const { login, followers_url } = response.data;
      axios(`${rootUrl}/users/${login}/repos?per_page=100`).then((response) =>
        setRepos(response.data)
      );
      // get followers
      axios(`${followers_url}?per_page=100`).then((response) =>
        setFollowers(response.data)
      );
    } else {
      // error msg
    }
  };

  //check Rate Limit
  const checkRequests = () => {
    axios(`${rootUrl}/rate_limit`)
      .then(({ data }) => {
        const {
          rate: { remaining },
        } = data;
        setRequest(remaining);
        if (remaining === 0) {
          toggleError(true, "sorry your horly rate limit over");
        }
      })
      .catch((error) => console.log(error));
  };

  function toggleError(show, msg) {
    setError({ show, msg });
  }

  useEffect(() => {
    checkRequests();
    // eslint-disable-next-line
  }, []);

  return (
    <GithubContext.Provider
      value={{ githubUser, repos, followers, request, searchUser, error }}
    >
      {children}
    </GithubContext.Provider>
  );
};

export const useGlobalContext = () => {
  return useContext(GithubContext);
};

export { GithubProvider, GithubContext };
