export default [
  {
    id: 671971633,
    node_id: "R_kgDOKA15MQ",
    name: "cars_showcase",
    full_name: "pradeepkumar-28/cars_showcase",
    private: false,
    owner: {
      login: "pradeepkumar-28",
      id: 92711745,
      node_id: "U_kgDOBYarQQ",
      avatar_url: "https://avatars.githubusercontent.com/u/92711745?v=4",
      gravatar_id: "",
      url: "https://api.github.com/users/pradeepkumar-28",
      html_url: "https://github.com/pradeepkumar-28",
      followers_url: "https://api.github.com/users/pradeepkumar-28/followers",
      following_url:
        "https://api.github.com/users/pradeepkumar-28/following{/other_user}",
      gists_url: "https://api.github.com/users/pradeepkumar-28/gists{/gist_id}",
      starred_url:
        "https://api.github.com/users/pradeepkumar-28/starred{/owner}{/repo}",
      subscriptions_url:
        "https://api.github.com/users/pradeepkumar-28/subscriptions",
      organizations_url: "https://api.github.com/users/pradeepkumar-28/orgs",
      repos_url: "https://api.github.com/users/pradeepkumar-28/repos",
      events_url:
        "https://api.github.com/users/pradeepkumar-28/events{/privacy}",
      received_events_url:
        "https://api.github.com/users/pradeepkumar-28/received_events",
      type: "User",
      site_admin: false,
    },
    html_url: "https://github.com/pradeepkumar-28/cars_showcase",
    description: null,
    fork: false,
    url: "https://api.github.com/repos/pradeepkumar-28/cars_showcase",
    forks_url:
      "https://api.github.com/repos/pradeepkumar-28/cars_showcase/forks",
    keys_url:
      "https://api.github.com/repos/pradeepkumar-28/cars_showcase/keys{/key_id}",
    collaborators_url:
      "https://api.github.com/repos/pradeepkumar-28/cars_showcase/collaborators{/collaborator}",
    teams_url:
      "https://api.github.com/repos/pradeepkumar-28/cars_showcase/teams",
    hooks_url:
      "https://api.github.com/repos/pradeepkumar-28/cars_showcase/hooks",
    issue_events_url:
      "https://api.github.com/repos/pradeepkumar-28/cars_showcase/issues/events{/number}",
    events_url:
      "https://api.github.com/repos/pradeepkumar-28/cars_showcase/events",
    assignees_url:
      "https://api.github.com/repos/pradeepkumar-28/cars_showcase/assignees{/user}",
    branches_url:
      "https://api.github.com/repos/pradeepkumar-28/cars_showcase/branches{/branch}",
    tags_url: "https://api.github.com/repos/pradeepkumar-28/cars_showcase/tags",
    blobs_url:
      "https://api.github.com/repos/pradeepkumar-28/cars_showcase/git/blobs{/sha}",
    git_tags_url:
      "https://api.github.com/repos/pradeepkumar-28/cars_showcase/git/tags{/sha}",
    git_refs_url:
      "https://api.github.com/repos/pradeepkumar-28/cars_showcase/git/refs{/sha}",
    trees_url:
      "https://api.github.com/repos/pradeepkumar-28/cars_showcase/git/trees{/sha}",
    statuses_url:
      "https://api.github.com/repos/pradeepkumar-28/cars_showcase/statuses/{sha}",
    languages_url:
      "https://api.github.com/repos/pradeepkumar-28/cars_showcase/languages",
    stargazers_url:
      "https://api.github.com/repos/pradeepkumar-28/cars_showcase/stargazers",
    contributors_url:
      "https://api.github.com/repos/pradeepkumar-28/cars_showcase/contributors",
    subscribers_url:
      "https://api.github.com/repos/pradeepkumar-28/cars_showcase/subscribers",
    subscription_url:
      "https://api.github.com/repos/pradeepkumar-28/cars_showcase/subscription",
    commits_url:
      "https://api.github.com/repos/pradeepkumar-28/cars_showcase/commits{/sha}",
    git_commits_url:
      "https://api.github.com/repos/pradeepkumar-28/cars_showcase/git/commits{/sha}",
    comments_url:
      "https://api.github.com/repos/pradeepkumar-28/cars_showcase/comments{/number}",
    issue_comment_url:
      "https://api.github.com/repos/pradeepkumar-28/cars_showcase/issues/comments{/number}",
    contents_url:
      "https://api.github.com/repos/pradeepkumar-28/cars_showcase/contents/{+path}",
    compare_url:
      "https://api.github.com/repos/pradeepkumar-28/cars_showcase/compare/{base}...{head}",
    merges_url:
      "https://api.github.com/repos/pradeepkumar-28/cars_showcase/merges",
    archive_url:
      "https://api.github.com/repos/pradeepkumar-28/cars_showcase/{archive_format}{/ref}",
    downloads_url:
      "https://api.github.com/repos/pradeepkumar-28/cars_showcase/downloads",
    issues_url:
      "https://api.github.com/repos/pradeepkumar-28/cars_showcase/issues{/number}",
    pulls_url:
      "https://api.github.com/repos/pradeepkumar-28/cars_showcase/pulls{/number}",
    milestones_url:
      "https://api.github.com/repos/pradeepkumar-28/cars_showcase/milestones{/number}",
    notifications_url:
      "https://api.github.com/repos/pradeepkumar-28/cars_showcase/notifications{?since,all,participating}",
    labels_url:
      "https://api.github.com/repos/pradeepkumar-28/cars_showcase/labels{/name}",
    releases_url:
      "https://api.github.com/repos/pradeepkumar-28/cars_showcase/releases{/id}",
    deployments_url:
      "https://api.github.com/repos/pradeepkumar-28/cars_showcase/deployments",
    created_at: "2023-07-28T15:19:29Z",
    updated_at: "2023-07-28T15:19:30Z",
    pushed_at: "2023-07-28T15:19:30Z",
    git_url: "git://github.com/pradeepkumar-28/cars_showcase.git",
    ssh_url: "git@github.com:pradeepkumar-28/cars_showcase.git",
    clone_url: "https://github.com/pradeepkumar-28/cars_showcase.git",
    svn_url: "https://github.com/pradeepkumar-28/cars_showcase",
    homepage: null,
    size: 0,
    stargazers_count: 0,
    watchers_count: 0,
    language: null,
    has_issues: true,
    has_projects: true,
    has_downloads: true,
    has_wiki: true,
    has_pages: false,
    has_discussions: false,
    forks_count: 0,
    mirror_url: null,
    archived: false,
    disabled: false,
    open_issues_count: 0,
    license: null,
    allow_forking: true,
    is_template: false,
    web_commit_signoff_required: false,
    topics: [],
    visibility: "public",
    forks: 0,
    open_issues: 0,
    watchers: 0,
    default_branch: "main",
  },
];
