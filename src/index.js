import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import { GithubProvider } from "./context/context";
import { Auth0Provider } from "@auth0/auth0-react";

//Domai:  dev-ww9wrsiy.us.auth0.com
//client ID: Z5Y38QKBnWo4ZQkCAptJ5pgp46q5S12R

ReactDOM.render(
  <React.StrictMode>
    <Auth0Provider
      domain="dev-ww9wrsiy.us.auth0.com"
      clientId="Z5Y38QKBnWo4ZQkCAptJ5pgp46q5S12R"
      redirectUri={window.location.origin}
      cacheLocation="localstorage"
    >
      <GithubProvider>
        <App />
      </GithubProvider>
    </Auth0Provider>
  </React.StrictMode>,
  document.getElementById("root")
);
